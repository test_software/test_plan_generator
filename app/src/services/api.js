/* Доделать */
import axios from "axios";

class Api {
	constructor(options = {}) {
		this.hostname = window.location.hostname;
		this.protocol = window.location.protocol;
		this.api_urls = {
			'default' : 'yaml/default.yml',
			'step'    : 'yaml/steps/'
		};

		this.axios = axios.create({
			baseURL : this.protocol + '://' + this.hostname + '/'
		});
	}


	/**
	 *
	 * @param options
	 * @param options.entity
	 * @param options.step_name
	 */
	get(options) {
		let url = this.api_urls[options.entity];

		if (options.entity == 'step') {
			url += options.step_name;
		}

		return this.axios.get(url);
	}
}

export default Api;