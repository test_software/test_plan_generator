import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state     : {
		snackbar : {
			visible : false,
			text    : '',
			color   : ''
		}
	},
	mutations : {
		SNACKBAR_SHOW      : (state) => {
			state.snackbar.visible = true;
		},
		SNACKBAR_HIDE      : (state) => {
			state.snackbar.visible = false;
			state.snackbar.color   = '';
			state.snackbar.text    = '';
		},
		SNACKBAR_SET_TEXT  : (state, payload) => {
			state.snackbar.text = payload;
		},
		SNACKBAR_SET_COLOR : (state, payload) => {
			state.snackbar.color = payload;
		}
	},
	actions   : {
		SNACKBAR_SUCCESS : (context, text) => {
			context.commit('SNACKBAR_SET_TEXT', text);
			context.commit('SNACKBAR_SET_COLOR', 'success');
			context.commit('SNACKBAR_SHOW');
			setTimeout(() => {
				context.commit('SNACKBAR_HIDE');
			}, 5000);
		},
		SNACKBAR_ERROR   : (context, text) => {
			context.commit('SNACKBAR_SET_TEXT', text);
			context.commit('SNACKBAR_SET_COLOR', 'error');
			context.commit('SNACKBAR_SHOW');
			setTimeout(() => {
				context.commit('SNACKBAR_HIDE');
			}, 5000);
		}
	},
	modules   : {}
})
