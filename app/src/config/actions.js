const DEFAULT_ACTIONS = {

	load_page : {
		action     : 'load_page',
		url        : 'bizon.help',
		screenshot : {
			file_name : 'image.png'
		}
	},

	wait : {
		action : 'wait',
		time   : 900
	},

	click : {
		action     : 'click',
		target     : {
			partial_link_text : 'текст'
		},
		screenshot : {
			file_name : 'image.png'
		}
	},

	wait_element_located : {
		action     : 'wait_element_located',
		target     : {
			partial_link_text : 'текст'
		},
		screenshot : {
			file_name : 'image.png'
		}
	}
};

export default DEFAULT_ACTIONS;