const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	publicPath            : './',
	outputDir             : 'public',
	chainWebpack          : config => {
		config.plugin('html')
		.tap(args => {
			args[0].template = './src/index.html';
			return args;
		})
	},
	transpileDependencies : [
		'vuetify'
	]
}
